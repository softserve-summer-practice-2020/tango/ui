import {TestModel} from './TestModel.js';
import {TestView} from './TestView.js';

export class TestController{
    constructor({subscribe, unsubscribe, notify}){
        this.model = new TestModel();
        this.view = new TestView();

        // Publisher methods
        this.subscribe = subscribe;
        this.unsubscribe = unsubscribe;
        this.notify = notify;

        this.onPageLoad();
    }

    onPageLoad = () => {
        this.view.render(this.model.pageTitle);
    }
}
